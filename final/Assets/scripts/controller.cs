﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class controller : MonoBehaviour {
    public Text ScoreText;
    public Text DeadScoreText;
    public Text DeadHighScoreText;
    static public int ScoreNum;
    private int highNum;

    void awake () {
        ScoreNum = 0;
        ScoreText = GameObject.Find("scoreText").GetComponent<Text>();
    }
	void Update () {

        highNum = PlayerPrefs.GetInt("HighScoreSaved");
        ScoreText.text = "SCORE: " + ScoreNum;
        DeadScoreText.text = "SCORE: " + ScoreNum;
        DeadHighScoreText.text = "high SCORE: " + highNum;

        if (ScoreNum > highNum)
        {
            PlayerPrefs.SetInt("HighScoreSaved", ScoreNum);
        }
    }

}