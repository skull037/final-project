﻿using UnityEngine;
using System.Collections;

public class weapon : MonoBehaviour
{
    private float randomPitch;
    public Rigidbody projectile;
    public Rigidbody Rifleprojectile;
    public Rigidbody Shotgunprojectile;

    static public bool IsPistol;
    static public bool IsRifle;
    static public bool IsSHotgun;
    private float fireTimer;

    void Awake()
    {
        IsPistol = true;
    }

    void Update()
    {
        randomPitch = Random.Range(1, 2);
        if (Input.GetButtonDown("Fire1")&& IsPistol == true)
        {
            GetComponent<AudioSource>().pitch = randomPitch;
            GetComponent<AudioSource>().Play();
            Rigidbody instantiatedProjectile = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(-20, 0, 0));
        }
        fireTimer++;
        randomPitch = Random.Range(1, 2);
        if (Input.GetButton("Fire1") && fireTimer > 0 && IsRifle == true)
        {
            GetComponent<AudioSource>().pitch = randomPitch;
            GetComponent<AudioSource>().Play();
            Rigidbody instantiatedProjectile = Instantiate(Rifleprojectile, transform.position, transform.rotation) as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(-20, 0, 0));
            fireTimer = -5;

        }
        randomPitch = Random.Range(1, 2);
        if (Input.GetButtonDown("Fire1") && IsSHotgun == true)
        {
            GetComponent<AudioSource>().pitch = randomPitch;
            GetComponent<AudioSource>().Play();

            Rigidbody instantiatedProjectile = Instantiate(Shotgunprojectile, transform.position, transform.rotation) as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(-20, 0, 0));

            instantiatedProjectile = Instantiate(Shotgunprojectile, transform.position, transform.rotation) as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(-20, 11, 0));

            instantiatedProjectile = Instantiate(Shotgunprojectile, transform.position, transform.rotation) as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(-20, -11, 0));
        }

    }
}
