﻿using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour {

    public GameObject[] spawnE;
    public GameObject[] spawnPower;
    public GameObject spawnerPower;
    static public int Ammount;
    static public int AmmountLimit;
    public GameObject[] Spawner;
    public int Difficulty;
    private GameObject[] enemies;
    private float lastUpdate;
    public int timeDiff;
    public string spawnerType;
    static public float PowerLimit;
    private float randomPitch;
    void Update() {
        randomPitch = Random.Range(1, 2);
        AmmountLimit = 25 + (Mathf.RoundToInt(Time.timeSinceLevelLoad / 4));
        if (Ammount <= AmmountLimit && spawnerType == "zombie")
        {
            GameObject RandomSpawn = Spawner[Random.Range(0, Spawner.Length)];
            GameObject RandomZed = spawnE[Random.Range(0, spawnE.Length)];

            Instantiate(RandomZed, (RandomSpawn.transform.position), RandomSpawn.transform.rotation);
            Ammount++;
        }
        PowerLimit = (Mathf.RoundToInt(Time.timeSinceLevelLoad));

        if(PowerLimit == 30 && spawnerType == "powerup" || PowerLimit == 130 && spawnerType == "powerup" || PowerLimit == 230 && spawnerType == "powerup")
        {
            GetComponent<AudioSource>().pitch = randomPitch;
            GetComponent<AudioSource>().Play();
            GameObject RandomSpawnPower = spawnPower[Random.Range(0, spawnPower.Length)];

            Instantiate(RandomSpawnPower, (spawnerPower.transform.position), spawnerPower.transform.rotation);
        }
    }
} 