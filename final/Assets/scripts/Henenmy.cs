﻿using UnityEngine;
using System.Collections;

public class Henenmy : MonoBehaviour
{
    private float randomPitch;
    public int ZedHealth = 2;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "death")
        {
            spawner.Ammount -= 1;
            Destroy(gameObject);
        }
        if (col.gameObject.tag == "bullet")
        {
            ZedHealth -= 1;
            GetComponent<AudioSource>().pitch = randomPitch;
            GetComponent<AudioSource>().Play();
        }
    }
    void Update()
    {
        if(ZedHealth <= 0)
        {
            Debug.Log("DIE");
            Destroy(gameObject);
        }
    }
}