﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class character : MonoBehaviour {
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    public int Playerhealth;
    public GameObject UIScreen;
    public GameObject DeadScreen;
    public Image DamageScreen;
    public Sprite Damage1;
    public Sprite Damage2;
    public Sprite Damage3;
    public Sprite Damage4;
    public Sprite Damage5;
    public GameObject PausedScreen;
    private bool paused;

    void start()
    {
        Playerhealth = 5;
        paused = false;
    }
    void Update()
    {
        pauseGame();
        if (Playerhealth <= 0)
        {
            UIScreen.SetActive(false);
            DeadScreen.SetActive(true);
            gameObject.SetActive(false);
        }
        else if (Playerhealth >= 5)
        {
            DamageScreen.sprite = Damage1;
        }
        else if (Playerhealth == 4)
        {
            DamageScreen.sprite = Damage2;
        }
        else  if (Playerhealth == 3)
        {
            DamageScreen.sprite = Damage3;
        }
        else if (Playerhealth == 2)
        {
            DamageScreen.sprite = Damage4;
        }
        else if (Playerhealth == 1)
        {
            DamageScreen.sprite = Damage5;
        }

        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
    public float speedA;

    void FixedUpdate()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitdist = 0.0f;
        if (playerPlane.Raycast(ray, out hitdist))
        {
            Vector3 targetPoint = ray.GetPoint(hitdist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speedA * Time.deltaTime);
        }
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "enemy")
        {
            Playerhealth -= 1;
        }
        if (col.gameObject.tag == "death")
        {
            Playerhealth--;
        }
        if (col.gameObject.tag == "giveRifle")
        {
            weapon.IsRifle = true;
            weapon.IsPistol = false;
            weapon.IsSHotgun = false;
            Destroy(col.gameObject);
        }
        if (col.gameObject.tag == "giveShotgun")
        {
            weapon.IsRifle = false;
            weapon.IsPistol = false;
            weapon.IsSHotgun = true;
            Destroy(col.gameObject);
        }
    }
    public void pauseGame()
    {
        if (Input.GetButtonDown("Cancel") && paused == false && Playerhealth >= 0)
        {
            PausedScreen.SetActive(true);
            Time.timeScale = 0;
            paused = true;
        }
        else if (Input.GetButtonDown("Cancel") && paused == true)
        {
            PausedScreen.SetActive(false);
            Time.timeScale = 1;
            paused = false;
        }
    }
}