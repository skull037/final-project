﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

    private int Kills = -1;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "enemy")
        {
            Destroy(col.gameObject);
            spawner.Ammount -= 1;
            Kills += 1;
            controller.ScoreNum += 50;
            controller.ScoreNum += 25 * Kills;
        }
        if (col.gameObject.tag == "Henemy")
        {
            Kills += 1;
            controller.ScoreNum += 50;
            controller.ScoreNum += 25 * Kills;
        }
        if (col.gameObject.tag == "terrain" || col.gameObject.tag == "Untagged")
        {
            Destroy(gameObject);
        }
        if (col.gameObject.tag == "death")
        {
            Destroy(gameObject);
        }
    }
}