﻿using UnityEngine;
using System.Collections;

public class gamemenu : MonoBehaviour {
    public GameObject UIScreen;
    public GameObject DeadScreen;
    public void startMenu()
    {
        spawner.Ammount = 0;
        controller.ScoreNum = 0;
        weapon.IsRifle = false;
        weapon.IsSHotgun = false;
        weapon.IsPistol = true;
        Application.LoadLevel(0);
    }
    public void RestartGame()
    {
        spawner.Ammount = 0;
        controller.ScoreNum = 0;
        UIScreen.SetActive(true);
        DeadScreen.SetActive(false);
        weapon.IsRifle = false;
        weapon.IsSHotgun = false;
        weapon.IsPistol = true;
        Application.LoadLevel(1);
    }
}