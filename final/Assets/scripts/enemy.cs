﻿using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour
{
    private float randomPitch;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "death")
        {
            spawner.Ammount -= 1;
            Destroy(gameObject);
        }
        if (col.gameObject.tag == "bullet")
        {
            GetComponent<AudioSource>().pitch = randomPitch;
            GetComponent<AudioSource>().Play();
        }
    }
}
